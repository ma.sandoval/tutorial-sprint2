const reasonInput= document.querySelector('#input-reason');
const amountInput= document.querySelector('#input-amount');
const cancelBtn= document.querySelector('#btn-cancel');
const confirmBtn=document.querySelector('#btn-confirm');
const expensesList= document.querySelector('#expenses-list');
const totalExpensesOutput= document.querySelector('#total-expenses');
const alertCtrl=document.querySelector('ion-alert-controller');
const totalAvailableOutput= document.querySelector('#total-available');
const budgetInput= document.querySelector('#input-budget');

let totalExpenses=0; 
let totalBudget=0;

const clear =()=>{
    reasonInput.value='';
    amountInput.value='';
    budgetInput.value='';
   
};
confirmBtn.addEventListener('click',()=>{
    const enteredReason=reasonInput.value; 
    const enteredAmount=amountInput.value; 
    const enteredBudget= budgetInput.value;
    totalAvailableOutput.textContent=0;
        if(enteredReason.trim().length<=0 || enteredAmount <=0 || enteredAmount.trim().length<=0 || enteredBudget.trim().length<=0|| enteredBudget<=0){
       alertCtrl.create({message:'Please enter a valid reason and amount!',
       header:'Invalid Inputs',
       buttons: ['Okay']
    }).then(alertElement =>{
        alertElement.present();
    });
        return;
    }
    const newItem=document.createElement('ion-item');
    newItem.textContent= enteredReason+ ': $'+enteredAmount;
    expensesList.appendChild(newItem);
    totalExpenses+=+enteredAmount;
    totalBudget=enteredBudget-enteredAmount;
    totalExpensesOutput.textContent=totalExpenses;
    totalAvailableOutput.textContent=totalBudget;
    clear();
}); 

cancelBtn.addEventListener('click',clear);
